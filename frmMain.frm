VERSION 5.00
Begin VB.Form frmMain 
   BorderStyle     =   1  '단일 고정
   Caption         =   "Auto Campus 0.0 (자동 수강신청)"
   ClientHeight    =   4695
   ClientLeft      =   150
   ClientTop       =   540
   ClientWidth     =   4455
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4695
   ScaleWidth      =   4455
   StartUpPosition =   2  '화면 가운데
   Begin VB.Frame fraTime 
      Height          =   375
      Left            =   240
      TabIndex        =   55
      Top             =   4200
      Width           =   3975
      Begin VB.Label lblTime 
         Alignment       =   2  '가운데 맞춤
         BackStyle       =   0  '투명
         Caption         =   "0000년 00월 00일 월요일 오전 00시 00분 00초"
         Height          =   255
         Left            =   120
         TabIndex        =   56
         Top             =   120
         Width           =   3735
      End
   End
   Begin VB.TextBox txtClass 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   2
      Left            =   3600
      MaxLength       =   2
      TabIndex        =   18
      Top             =   1080
      Width           =   375
   End
   Begin VB.TextBox txtNum 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   2
      Left            =   2760
      MaxLength       =   5
      TabIndex        =   17
      Top             =   1080
      Width           =   735
   End
   Begin VB.TextBox txtName 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   2
      Left            =   480
      MaxLength       =   15
      TabIndex        =   16
      Top             =   1080
      Width           =   2175
   End
   Begin VB.TextBox txtClass 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   3
      Left            =   3600
      MaxLength       =   2
      TabIndex        =   23
      Top             =   1440
      Width           =   375
   End
   Begin VB.TextBox txtNum 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   3
      Left            =   2760
      MaxLength       =   5
      TabIndex        =   22
      Top             =   1440
      Width           =   735
   End
   Begin VB.TextBox txtName 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   3
      Left            =   480
      MaxLength       =   15
      TabIndex        =   21
      Top             =   1440
      Width           =   2175
   End
   Begin VB.TextBox txtClass 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   4
      Left            =   3600
      MaxLength       =   2
      TabIndex        =   28
      Top             =   1800
      Width           =   375
   End
   Begin VB.TextBox txtNum 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   4
      Left            =   2760
      MaxLength       =   5
      TabIndex        =   27
      Top             =   1800
      Width           =   735
   End
   Begin VB.TextBox txtName 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   4
      Left            =   480
      MaxLength       =   15
      TabIndex        =   26
      Top             =   1800
      Width           =   2175
   End
   Begin VB.TextBox txtClass 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   5
      Left            =   3600
      MaxLength       =   2
      TabIndex        =   33
      Top             =   2160
      Width           =   375
   End
   Begin VB.TextBox txtNum 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   5
      Left            =   2760
      MaxLength       =   5
      TabIndex        =   32
      Top             =   2160
      Width           =   735
   End
   Begin VB.TextBox txtName 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   5
      Left            =   480
      MaxLength       =   15
      TabIndex        =   31
      Top             =   2160
      Width           =   2175
   End
   Begin VB.TextBox txtClass 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   6
      Left            =   3600
      MaxLength       =   2
      TabIndex        =   38
      Top             =   2520
      Width           =   375
   End
   Begin VB.TextBox txtNum 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   6
      Left            =   2760
      MaxLength       =   5
      TabIndex        =   37
      Top             =   2520
      Width           =   735
   End
   Begin VB.TextBox txtName 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   6
      Left            =   480
      MaxLength       =   15
      TabIndex        =   36
      Top             =   2520
      Width           =   2175
   End
   Begin VB.TextBox txtClass 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   7
      Left            =   3600
      MaxLength       =   2
      TabIndex        =   43
      Top             =   2880
      Width           =   375
   End
   Begin VB.TextBox txtNum 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   7
      Left            =   2760
      MaxLength       =   5
      TabIndex        =   42
      Top             =   2880
      Width           =   735
   End
   Begin VB.TextBox txtName 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   7
      Left            =   480
      MaxLength       =   15
      TabIndex        =   41
      Top             =   2880
      Width           =   2175
   End
   Begin VB.TextBox txtClass 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   8
      Left            =   3600
      MaxLength       =   2
      TabIndex        =   48
      Top             =   3240
      Width           =   375
   End
   Begin VB.TextBox txtNum 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   8
      Left            =   2760
      MaxLength       =   5
      TabIndex        =   47
      Top             =   3240
      Width           =   735
   End
   Begin VB.TextBox txtName 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   8
      Left            =   480
      MaxLength       =   15
      TabIndex        =   46
      Top             =   3240
      Width           =   2175
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "."
      Height          =   255
      Index           =   2
      Left            =   4080
      TabIndex        =   19
      Top             =   1080
      Width           =   255
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "."
      Height          =   255
      Index           =   3
      Left            =   4080
      TabIndex        =   24
      Top             =   1440
      Width           =   255
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "."
      Height          =   255
      Index           =   4
      Left            =   4080
      TabIndex        =   29
      Top             =   1800
      Width           =   255
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "."
      Height          =   255
      Index           =   5
      Left            =   4080
      TabIndex        =   34
      Top             =   2160
      Width           =   255
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "."
      Height          =   255
      Index           =   6
      Left            =   4080
      TabIndex        =   39
      Top             =   2520
      Width           =   255
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "."
      Height          =   255
      Index           =   7
      Left            =   4080
      TabIndex        =   44
      Top             =   2880
      Width           =   255
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "."
      Height          =   255
      Index           =   8
      Left            =   4080
      TabIndex        =   49
      Top             =   3240
      Width           =   255
   End
   Begin VB.TextBox txtClass 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   1
      Left            =   3600
      MaxLength       =   2
      TabIndex        =   13
      Top             =   720
      Width           =   375
   End
   Begin VB.TextBox txtNum 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   1
      Left            =   2760
      MaxLength       =   5
      TabIndex        =   12
      Top             =   720
      Width           =   735
   End
   Begin VB.TextBox txtName 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   1
      Left            =   480
      MaxLength       =   15
      TabIndex        =   11
      Top             =   720
      Width           =   2175
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "."
      Height          =   255
      Index           =   1
      Left            =   4080
      TabIndex        =   14
      Top             =   720
      Width           =   255
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "."
      Height          =   255
      Index           =   0
      Left            =   4080
      TabIndex        =   9
      Top             =   360
      Width           =   255
   End
   Begin VB.TextBox txtName 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   0
      Left            =   480
      MaxLength       =   15
      TabIndex        =   6
      Top             =   360
      Width           =   2175
   End
   Begin VB.TextBox txtNum 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   0
      Left            =   2760
      MaxLength       =   5
      TabIndex        =   7
      Top             =   360
      Width           =   735
   End
   Begin VB.TextBox txtClass 
      Alignment       =   2  '가운데 맞춤
      Height          =   270
      Index           =   0
      Left            =   3600
      MaxLength       =   2
      TabIndex        =   8
      Top             =   360
      Width           =   375
   End
   Begin VB.CommandButton cmdClearAll 
      Caption         =   "A"
      Height          =   255
      Left            =   4080
      TabIndex        =   53
      Top             =   3600
      Width           =   255
   End
   Begin VB.CommandButton cmdLogin 
      Caption         =   "로그인 설정"
      Height          =   255
      Left            =   2760
      TabIndex        =   52
      Top             =   3600
      Width           =   1215
   End
   Begin VB.CheckBox chkSearch 
      Caption         =   "[단축키]로 해당 과목번호의 강의시간표 검색"
      Height          =   180
      Left            =   240
      TabIndex        =   54
      Top             =   3960
      Width           =   3975
   End
   Begin VB.Label lblLabels 
      Caption         =   "단축키"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   615
   End
   Begin VB.Label lblHotkey 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "[1]"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Top             =   405
      Width           =   255
   End
   Begin VB.Label lblHotkey 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "[2]"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   10
      Top             =   765
      Width           =   255
   End
   Begin VB.Label lblHotkey 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "[3]"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   15
      Top             =   1125
      Width           =   255
   End
   Begin VB.Label lblHotkey 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "[4]"
      Height          =   255
      Index           =   3
      Left            =   120
      TabIndex        =   20
      Top             =   1485
      Width           =   255
   End
   Begin VB.Label lblHotkey 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "[5]"
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   25
      Top             =   1845
      Width           =   255
   End
   Begin VB.Label lblHotkey 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "[6]"
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   30
      Top             =   2205
      Width           =   255
   End
   Begin VB.Label lblHotkey 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "[7]"
      Height          =   255
      Index           =   6
      Left            =   120
      TabIndex        =   35
      Top             =   2565
      Width           =   255
   End
   Begin VB.Label lblHotkey 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "[8]"
      Height          =   255
      Index           =   7
      Left            =   120
      TabIndex        =   40
      Top             =   2925
      Width           =   255
   End
   Begin VB.Label lblHotkey 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "[9]"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   45
      Top             =   3285
      Width           =   255
   End
   Begin VB.Label lblHotkey 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "[0]"
      Height          =   255
      Index           =   9
      Left            =   120
      TabIndex        =   50
      Top             =   3645
      Width           =   255
   End
   Begin VB.Label lblLabels 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "삭제"
      Height          =   255
      Index           =   4
      Left            =   3960
      TabIndex        =   4
      Top             =   120
      Width           =   495
   End
   Begin VB.Label lblLabels 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "분반"
      Height          =   255
      Index           =   3
      Left            =   3600
      TabIndex        =   3
      Top             =   120
      Width           =   375
   End
   Begin VB.Label lblLabels 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "과목번호"
      Height          =   255
      Index           =   2
      Left            =   2760
      TabIndex        =   2
      Top             =   120
      Width           =   735
   End
   Begin VB.Label lblLabels 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "교과목명"
      Height          =   255
      Index           =   1
      Left            =   480
      TabIndex        =   1
      Top             =   120
      Width           =   2175
   End
   Begin VB.Label lblLogin 
      Alignment       =   2  '가운데 맞춤
      Caption         =   "자동 로그인 비활성화"
      Height          =   255
      Left            =   480
      TabIndex        =   51
      Top             =   3640
      Width           =   2175
   End
   Begin VB.Menu MnuStart 
      Caption         =   "시작[F2]"
   End
   Begin VB.Menu mnuFile 
      Caption         =   "저장/불러오기"
      Begin VB.Menu mnuSave 
         Caption         =   "저장하기"
      End
      Begin VB.Menu mnuLoad 
         Caption         =   "불러오기"
      End
      Begin VB.Menu mnuDelete 
         Caption         =   "삭제하기"
      End
   End
   Begin VB.Menu mnuTrinity 
      Caption         =   "트리니티"
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "정보"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim i As Integer

Dim Start As Boolean
Public UserID As String
Dim Password As String

Private Sub fnSave()
    Dim StrTemp As String
    
    Dim FileNum As Integer '파일번호 변수 선언*
    FileNum = FreeFile '파일번호 지정*
    
    If Dir(FilePath) <> "" Then '기존 파일 삭제
        Kill FilePath
    End If
    
    Open FilePath For Binary As #FileNum '파일 열기*
    
    StrTemp = ""
    For i = 0 To 8 '과목 정보 저장
        StrTemp = StrTemp & txtName(i).Text & "," & txtNum(i).Text & "," & txtClass(i).Text & vbCrLf
    Next i
    
    StrTemp = StrTemp & UserID '사용자ID 저장
    StrTemp = StrTemp & ", " & chkSearch '강의시간표 검색
    
    Put #FileNum, 1, StrTemp '파일 내용 저장(1은 레코드 번호)*
    
    Close #FileNum '파일 닫기*
End Sub

Private Sub fnLoad()
    Dim StrTemp As String
    Dim ArrTemp() As String
    
    Dim FileNum As Integer '파일번호 변수 선언*
    
    If Dir(FilePath) <> "" Then '파일 존재 확인*
        FileNum = FreeFile '파일번호 지정*
        
        Open FilePath For Input As #FileNum '파일 열기*

        'Do While Not EOF(FileNum) '내용이 없을 때 까지 반복*
        'Loop
        
        For i = 0 To 8 '과목 정보 불러오기
            Line Input #FileNum, StrTemp
            ArrTemp = Split(StrTemp, ",") '(,)단위로 끊기
            
            txtName(i).Text = ArrTemp(0)
            txtNum(i).Text = ArrTemp(1)
            txtClass(i).Text = ArrTemp(2)
        Next i
        
        Line Input #FileNum, StrTemp
        ArrTemp = Split(StrTemp, ",")
        
        UserID = ArrTemp(0) ''사용자ID 불러오기
        chkSearch = ArrTemp(1) '강의시간표 검색
        
        Close #FileNum '파일 닫기*
    End If
End Sub

Private Sub fnStart()
    If Not Start And fnCheck("시작") = True Then '입력 확인 후 시작
        Start = True
        
        MnuStart.Caption = "중지[F2]"
        
        mnuSave.Enabled = False
        mnuLoad.Enabled = False
        
        For i = 0 To 8 '활성화
            txtName(i).Enabled = False
            txtNum(i).Enabled = False
            txtClass(i).Enabled = False
            cmdClear(i).Enabled = False
        Next i
        
        chkSearch.Enabled = False
        cmdClearAll.Enabled = False
        cmdLogin.Enabled = False
    ElseIf Start Then '중지
        Start = False
        
        MnuStart.Caption = "시작[F2]"
       
        mnuSave.Enabled = True
        mnuLoad.Enabled = True
        
        For i = 0 To 8 '비활성화
            txtName(i).Enabled = True
            txtNum(i).Enabled = True
            txtClass(i).Enabled = True
            cmdClear(i).Enabled = True
        Next i
        
        chkSearch.Enabled = True
        cmdClearAll.Enabled = True
        cmdLogin.Enabled = True
    End If
End Sub

Private Function fnCheck(strCheck As String)
    Dim Check As Boolean
    Dim StrTemp As String
    
    Check = False
    StrTemp = "<" & strCheck & "을 할 수 없습니다.>" & vbCrLf
    
    For i = 0 To 8 '과목 정보 입력 확인
        If txtName(i).Text <> "" Or txtNum(i).Text <> "" Or txtClass(i).Text <> "" Then
            Check = True
            Exit For
        End If
    Next i
    
    If Check Then '과목 정보가 있으면
        For i = 0 To 8
            If txtName(i).Text = "" And (txtNum(i).Text <> "" Or txtClass(i).Text <> "") Then
                StrTemp = StrTemp & vbCrLf & "단축키 [" & i + 1 & "] : 교과목명을 입력하세요."
                Check = False
            End If
            
            If (txtName(i).Text <> "" Or txtClass(i).Text <> "") And txtNum(i).Text = "" Then
                StrTemp = StrTemp & vbCrLf & "단축키 [" & i + 1 & "] : 과목번호를 입력하세요."
                Check = False
            End If
            
            If (txtName(i).Text <> "" Or txtNum(i).Text <> "") And txtClass(i).Text = "" Then
                StrTemp = StrTemp & vbCrLf & "단축키 [" & i + 1 & "] : 분반을 입력하세요."
                Check = False
            End If
            
            If txtNum(i).Text <> "" And (IsNumeric(txtNum(i).Text) = False Or _
                Left(txtNum(i).Text, 1) = " " Or Right(txtNum(i).Text, 1) = " ") Then
                StrTemp = StrTemp & vbCrLf & "단축키 [" & i + 1 & "] : 과목번호에 숫자만 입력할 수 있습니다."
                Check = False
            End If
            
            If txtClass(i).Text <> "" And (IsNumeric(txtClass(i).Text) = False Or _
                Left(txtClass(i).Text, 1) = " " Or Right(txtClass(i).Text, 1) = " ") Then
                StrTemp = StrTemp & vbCrLf & "단축키 [" & i + 1 & "] : 분반에 숫자만 입력할 수 있습니다."
                Check = False
            End If
        Next i
    Else '과목 정보가 하나도 없으면
        StrTemp = StrTemp & vbCrLf & "수강신청 과목 정보를 입력하세요."
        txtName(0).SetFocus
    End If
    
    If Not Check Then
        MsgBox StrTemp, vbExclamation, AppTitle
    End If
    
    fnCheck = Check
End Function

Public Sub fnLogin(ID As String, PW As String)
    UserID = ID
    Password = PW
    
    If Password <> "" Then '자동 로그인 활성화
        lblLogin.Caption = "사용자ID : " & UserID
        cmdLogin.Caption = "로그인 취소"
    End If
End Sub

Private Sub cmdClear_Click(Index As Integer)
    txtName(Index).Text = "" '내용 지우기
    txtNum(Index).Text = ""
    txtClass(Index).Text = ""
    
    txtName(Index).SetFocus
End Sub

Private Sub cmdClearAll_Click()
    For i = 0 To 8 '내용 모두 지우기
        txtName(i).Text = ""
        txtNum(i).Text = ""
        txtClass(i).Text = ""
    Next i
    
    txtName(0).SetFocus
End Sub

Private Sub cmdEnd_Click()
    End '이벤트 및 프로그램 종료
End Sub

Private Sub cmdLogin_Click()
    If Password = "" Then '자동 로그인 활성화
        frmLogin.Show vbModal
    Else '자동 로그인 비활성화
        'UserID = ""
        Password = ""
        
        lblLogin.Caption = "자동 로그인 비활성화"
        cmdLogin.Caption = "로그인 설정"
        
        Unload frmLogin
    End If
End Sub

Private Sub Form_Activate()
    Do '*
        DoEvents '다른 이벤트들을 실행할 수 있도록 루프를 잠시 멈춥니다.*
        Sleep 1 '다른 프로그램들을 실행할 수 있도록 루프를 잠시 멈춥니다.*
        
        lblTime.Caption = Format(Now, "yyyy년 m월 d일 aaaa ampm h시 n분 s초") '현재 시각
        
        If KeyPress = GetAsyncKeyState(vbKeyF2) Then '시작/중지 단축키
            fnStart
        End If
        
        If Start = True Then '시작 확인
            If KeyPress = GetAsyncKeyState(vbKey1) Then '단축키 [1]
                If txtNum(0).Text <> "" Then '과목번호가 있으면 실행
                    SendKeys "{HOME}+{END}{DELETE}" '기존 내용 지우기(+:Shift)
                    SendKeys txtNum(0).Text '과목번호 입력
                    If chkSearch.Value = 0 Then '수강신청
                        SendKeys "{TAB}" '다음 칸으로 이동
                        SendKeys "{HOME}+{END}{DELETE}" '기존 내용 지우기
                        SendKeys txtClass(0).Text '분반번호 입력
                    Else '강의시간표 검색
                        SendKeys "{ENTER}" '[Enter]를 눌러서 검색
                    End If
                End If
            ElseIf KeyPress = GetAsyncKeyState(vbKey2) Then '단축키 [2]
                If txtNum(1).Text <> "" Then
                    SendKeys "{HOME}+{END}{DELETE}"
                    SendKeys txtNum(1).Text
                    If chkSearch.Value = 0 Then
                        SendKeys "{TAB}"
                        SendKeys "{HOME}+{END}{DELETE}"
                        SendKeys txtClass(1).Text
                    Else
                        SendKeys "{ENTER}"
                    End If
                End If
            ElseIf KeyPress = GetAsyncKeyState(vbKey3) Then '단축키 [3]
                If txtNum(2).Text <> "" Then
                    SendKeys "{HOME}+{END}{DELETE}"
                    SendKeys txtNum(2).Text
                    If chkSearch.Value = 0 Then
                        SendKeys "{TAB}"
                        SendKeys "{HOME}+{END}{DELETE}"
                        SendKeys txtClass(2).Text
                    Else
                        SendKeys "{ENTER}"
                    End If
                End If
            ElseIf KeyPress = GetAsyncKeyState(vbKey4) Then '단축키 [4]
                If txtNum(3).Text <> "" Then
                    SendKeys "{HOME}+{END}{DELETE}"
                    SendKeys txtNum(3).Text
                     If chkSearch.Value = 0 Then
                        SendKeys "{TAB}"
                        SendKeys "{HOME}+{END}{DELETE}"
                        SendKeys txtClass(3).Text
                    Else
                        SendKeys "{ENTER}"
                    End If
                End If
            ElseIf KeyPress = GetAsyncKeyState(vbKey5) Then '단축키 [5]
                If txtNum(4).Text <> "" Then
                    SendKeys "{HOME}+{END}{DELETE}"
                    SendKeys txtNum(4).Text
                    If chkSearch.Value = 0 Then
                        SendKeys "{TAB}"
                        SendKeys "{HOME}+{END}{DELETE}"
                        SendKeys txtClass(4).Text
                    Else
                        SendKeys "{ENTER}"
                    End If
                End If
            ElseIf KeyPress = GetAsyncKeyState(vbKey6) Then '단축키 [6]
                If txtNum(5).Text <> "" Then
                    SendKeys "{HOME}+{END}{DELETE}"
                    SendKeys txtNum(5).Text
                    If chkSearch.Value = 0 Then
                        SendKeys "{TAB}"
                        SendKeys "{HOME}+{END}{DELETE}"
                        SendKeys txtClass(5).Text
                    Else
                        SendKeys "{ENTER}"
                    End If
                End If
            ElseIf KeyPress = GetAsyncKeyState(vbKey7) Then '단축키 [7]
                If txtNum(6).Text <> "" Then
                    SendKeys "{HOME}+{END}{DELETE}"
                    SendKeys txtNum(6).Text
                     If chkSearch.Value = 0 Then
                        SendKeys "{TAB}"
                        SendKeys "{HOME}+{END}{DELETE}"
                        SendKeys txtClass(6).Text
                    Else
                        SendKeys "{ENTER}"
                    End If
                End If
            ElseIf KeyPress = GetAsyncKeyState(vbKey8) Then '단축키 [8]
                If txtNum(7).Text <> "" Then
                    SendKeys "{HOME}+{END}{DELETE}"
                    SendKeys txtNum(7).Text
                     If chkSearch.Value = 0 Then
                        SendKeys "{TAB}"
                        SendKeys "{HOME}+{END}{DELETE}"
                        SendKeys txtClass(7).Text
                    Else
                        SendKeys "{ENTER}"
                    End If
                End If
            ElseIf KeyPress = GetAsyncKeyState(vbKey9) Then '단축키 [9]
                If txtNum(8).Text <> "" Then
                    SendKeys "{HOME}+{END}{DELETE}"
                    SendKeys txtNum(8).Text
                     If chkSearch.Value = 0 Then
                        SendKeys "{TAB}"
                        SendKeys "{HOME}+{END}{DELETE}"
                        SendKeys txtClass(8).Text
                    Else
                        SendKeys "{ENTER}"
                    End If
                End If
            ElseIf KeyPress = GetAsyncKeyState(vbKey0) Then '단축키 [0]
                If Password <> "" Then
                    SendKeys "{HOME}+{END}{DELETE}"
                    SendKeys UserID
                    SendKeys "{TAB}"
                    SendKeys "{HOME}+{END}{DELETE}"
                    SendKeys Password
                    SendKeys "{ENTER}"
                End If
            End If
        End If
    Loop '*
End Sub

Private Sub Form_Load()
    Me.Caption = AppTitle & " (자동 수강신청)"
    
    Start = False '중지
    
    fnLoad '파일 불러오기
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End '이벤트 및 프로그램 종료
End Sub

Private Sub mnuAbout_Click()
    MsgBox AppTitle & vbCrLf & vbCrLf & "For Trinity of Catohlic Univ." & vbCrLf & _
        vbCrLf & "Programmed by Serenity", vbInformation, AppTitle
End Sub

Private Sub mnuEnd_Click()
    End '이벤트 및 프로그램 종료
End Sub

Private Sub mnuDelete_Click()
    Dim YesNo As Integer
    
    YesNo = MsgBox("저장된 데이터를 삭제합니다.", vbYesNo, AppTitle)
    If YesNo = vbYes Then
        If Dir(FilePath) <> "" Then '기존 파일 삭제
            Kill FilePath
        Else
            MsgBox "저장된 데이터가 없습니다. ", vbExclamation, AppTitle
        End If
        
        cmdClearAll_Click
        
        UserID = ""
        Password = ""
    End If
End Sub

Private Sub mnuLoad_Click()
    Dim YesNo As Integer
    
    If Dir(FilePath) <> "" Then
        YesNo = MsgBox("저장된 데이터를 불러옵니다.", vbYesNo, AppTitle)
        
        If YesNo = vbYes Then
            fnLoad '파일 불러오기
        End If
    Else
        MsgBox "저장된 파일이 없습니다.", vbExclamation, AppTitle
    End If
End Sub

Private Sub mnuSave_Click()
    If fnCheck("저장") Then
        fnSave '파일 저장하기
        MsgBox "저장하기가 완료 되었습니다.", vbExclamation, AppTitle
    End If
End Sub

Private Sub MnuStart_Click()
    fnStart
End Sub

Private Sub mnuTrinity_Click()
    Shell "explorer http://portal.catholic.ac.kr/"
End Sub
