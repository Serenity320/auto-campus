VERSION 5.00
Begin VB.Form frmLogin 
   BorderStyle     =   1  '단일 고정
   Caption         =   "Auto Campus 0.0"
   ClientHeight    =   1335
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   2535
   Icon            =   "frmLogin.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1335
   ScaleWidth      =   2535
   StartUpPosition =   1  '소유자 가운데
   Begin VB.TextBox txtUserID 
      Height          =   270
      IMEMode         =   8  '영문
      Left            =   960
      MaxLength       =   12
      TabIndex        =   2
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox txtPassword 
      Height          =   270
      IMEMode         =   3  '사용 못함
      Left            =   960
      MaxLength       =   12
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   480
      Width           =   1455
   End
   Begin VB.CommandButton cmdLogin 
      Caption         =   "자동 로그인 설정"
      Default         =   -1  'True
      Height          =   375
      Left            =   240
      TabIndex        =   4
      Top             =   840
      Width           =   2055
   End
   Begin VB.Label lblUserID 
      Caption         =   "사용자ID"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   165
      Width           =   735
   End
   Begin VB.Label lblPassword 
      Caption         =   "비밀번호"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   525
      Width           =   735
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdLogin_Click()
    If txtUserID.Text = "" Then
        MsgBox "사용자ID을 입력하세요.", vbOKOnly, AppTitle
        txtUserID.SetFocus
    ElseIf txtPassword.Text = "" Then
        MsgBox "비밀번호를 입력하세요.", vbOKOnly, AppTitle
        txtPassword.SetFocus
    Else
        Call frmMain.fnLogin(txtUserID.Text, txtPassword.Text)
        Me.Hide
    End If
End Sub

Private Sub Form_Activate()
    If txtUserID <> "" Then
        txtPassword.SetFocus
    End If
End Sub

Private Sub Form_Load()
    Me.Caption = AppTitle
    
    txtUserID = frmMain.UserID
End Sub
